package ru.t1.chubarov.tm.repository;

import ru.t1.chubarov.tm.api.repository.IProjectRepository;
import ru.t1.chubarov.tm.model.Project;

public final class ProjectRepository extends AbstractUserOwnerRepository<Project> implements IProjectRepository {

}
