package ru.t1.chubarov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.chubarov.tm.enumerated.Role;
import ru.t1.chubarov.tm.exception.AbstractException;
import ru.t1.chubarov.tm.model.User;
import ru.t1.chubarov.tm.util.TerminalUtil;

public final class UserRegistryCommand extends AbstractUserCommand {

    @NotNull
    private final String NAME = "user-registry";
    @NotNull
    private final String DESCRIPTION = "Registry user.";

    @Override
    public void execute() throws AbstractException {
        System.out.println("[USER REGISTRY]");
        System.out.println("[ENTER LOGIN]");
        @NotNull final String login = TerminalUtil.nextLine();
        System.out.println("[ENTER PASSWORD]");
        @NotNull final String password = TerminalUtil.nextLine();
        System.out.println("[ENTER EMAIL]");
        @NotNull final String email = TerminalUtil.nextLine();
        @NotNull final User user = serviceLocator.getUserService().create(login, password, email);
        showUser(user);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return null;
    }

}
